(function() {

const ROOT = window.hasOwnProperty("_rootPath") ? window._rootPath : "/";
const TEMPLATES_PATH = ROOT + "templates/";
const TEMPLATE_FILE_EXTENSION = ".html";
const PAGE_TEMPLATE_NAME = "main";
const CONTENT_ELEMENT_ID = "content";
window._loadedStrings = {};
window._export = function(str) {
    // Hack.
    window._loadedStrings[filename(basename(document.currentScript.src))] = str[0];
};

function basename(path) {
    return path.replace(/.*\/([^\/]*)/, (m, o) => o);
}

function filename(basename) {
    return basename.replace(/(.*)\.[^\.]*/, (m, o) => o);
}

function nodeName(elem, name) {
    return elem.nodeName && elem.nodeName.toUpperCase() === name.toUpperCase();
}

// Modified from https://stackoverflow.com/questions/2592092/executing-script-elements-inserted-with-innerhtml/3250386#3250386
function hydrateNewContent(bodyElement) {
    // Finds and executes scripts in a newly added element's body.
    // Needed since innerHTML does not run scripts.
    //
    // Argument bodyElement is an element in the dom.
    function evalScript(elem) {
        var data = (elem.text || elem.textContent || elem.innerHTML || ""),
            src = elem.getAttribute("src"),
            head = document.getElementsByTagName("head")[0] || document.documentElement,
            script = document.createElement("script");

        script.type = "text/javascript";
        if (src) {
            script.setAttribute("src", src);
        }
        try {
            // doesn't work on ie...
            script.appendChild(document.createTextNode(data));      
        } catch(e) {
            // IE has funky script nodes
            script.text = data;
        }

        head.insertBefore(script, head.firstChild);
        head.removeChild(script);
    }

    function walk(node, func) {
        const children = node.childNodes;
        for (child of children) {
            func(child);
            walk(child, func);
        }
    }

    // main section of function
    var scripts = [],
        toPopulate = [],
        children = bodyElement.childNodes,
        script,
        i;

    walk(bodyElement, function(node) {
        if (nodeName(node, "script") && (!node.type || node.type.toLowerCase() === "text/javascript")) {
            scripts.push(node);
        }
        try {
            if (node.hasAttribute("data-template")) {
                toPopulate.push(node);
            }
        } catch (e) {
        }
    });

    for (var i = 0; scripts[i]; i++) {
        script = scripts[i];
        if (script.parentNode) {
            script.parentNode.removeChild(script);
        }
        evalScript(scripts[i]);
    }

    for (var i = 0; toPopulate[i]; ++i) {
        elementToPopulate = toPopulate[i];
        var templateName = elementToPopulate.getAttribute("data-template");
        loadContent(TEMPLATES_PATH + templateName + TEMPLATE_FILE_EXTENSION, elementToPopulate);
    }
}

function loadContent(path, element) {
    return loadString(path).then(str => {
        element.innerHTML = str;
        hydrateNewContent(element);
    }).catch(e => console.log(e));
}

function loadDependency(dependency) {
    return new Promise((resolve, reject) => {
        // TODO abstract this
        const script = document.createElement("script");
        script.setAttribute("src", _rootPath + dependency);
        script.onload = () => resolve();
        script.onerror = () => reject();
        document.head.appendChild(script);
    });
}

function loadString(path) {
    return new Promise((resolve, reject) => {
        const name = filename(basename(path));
        const script = document.createElement("script");
        script.src = path;
        script.onload = () => resolve(window._loadedStrings[name]);
        script.onerror = () => reject();
        document.head.appendChild(script);
    });
}

function populateTemplateWith(content) {
    const contentNode = document.getElementById(CONTENT_ELEMENT_ID);
    if (typeof content === "string") {
        contentNode.innerHTML = content;
    } else {
        for (node of content) {
            contentNode.appendChild(node);
        }
    }
    hydrateNewContent(contentNode);
}

function renderPage() {
    const oldContentNodes = Array.prototype.slice.call(document.body.childNodes, 0);
    let markdownMode = false;
    let contentHtml = "";
    for (node of oldContentNodes) {
        // Allow exactly one pre element
        if (nodeName(node, "pre")) {
            if (!markdownMode) {
                markdownMode = true;
                contentHtml = marked(node.innerHTML);
            } else {
                markdownMode = false;
                break;
            }
        // and any number of script elements
        } else if (!nodeName(node, "script")) {
            markdownMode = false;
            break;
        }
    }
    loadContent(TEMPLATES_PATH + PAGE_TEMPLATE_NAME + TEMPLATE_FILE_EXTENSION, document.body).then(() => {
        populateTemplateWith(markdownMode ? contentHtml : oldContentNodes);
        window._pageUpgradeComplete = true;
        window.dispatchEvent(new Event("pageUpgradeComplete"));
    });
}

console.log("Loader running...");
loadDependency("scripts/marked.min.js").then(() => {
    renderPage();
});

})();
